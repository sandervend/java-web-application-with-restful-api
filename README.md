# README #
It is a basic backend for a basic social media site based on emotions.

It is written in Java and makes use of the RESTful API.

This project uses the following frameworks:
Gradle, JUnit, Hamcrest, JSONAssert, MySQL (Why not HSQLDB? Because the project was already running MySQL), Hibernate, Jersey, Grizzly, JRebel (Not really a framework but mentioned anyways) and Auth0

### What is the point of this?  ###
This project is made public to showcase my abilities to write and showcase my skills as a student of Computer Sciences and my ability to write backend applications which make use of the RESTful API on JAVA.

### How do I get set up? ###

For now the server class must be run manually however a gradle wrapper run task will be implemented soon.