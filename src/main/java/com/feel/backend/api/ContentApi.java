package com.feel.backend.api;

import com.feel.backend.controller.ContentController;
import com.feel.backend.model.Content;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.glassfish.jersey.server.JSONP;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by S on 09.04.2016.
 */

@Path("content")
@Produces(MediaType.APPLICATION_JSON)
public class ContentApi {
    private static final String ITEMS_URL = "/api/content";

    @GET
    @JSONP(queryParam = "callback")
    public String getAllContents(@QueryParam("callback") String callback) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_DEFAULT);
        List<Content> contents = ContentController.getInstance().getAllContent();
        for (Content Content : contents) {
            Content.setLink(ITEMS_URL + "/" + Content.getId());
        }
        return mapper.writeValueAsString(contents);
    }

    @DELETE
    @JSONP(queryParam = "callback")
    public void deleteAllContents() throws Exception {
        ContentController.getInstance().deleteAllContent();
    }

    @GET
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public String getContent(@PathParam("id") int id) throws Exception {
        Content Content = ContentController.getInstance().getContent(id);
        return new ObjectMapper().writeValueAsString(Content);
    }

    @PUT
    @JSONP(queryParam = "callback")
    public void putContent(String ContentJson) throws Exception {
        Content content = new ObjectMapper().readValue(ContentJson, Content.class);
        ContentController.getInstance().saveOrUpdate(content);
    }

    @DELETE
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public void deleteContent(@PathParam("id") int id) throws Exception {
        ContentController.getInstance().deleteContent(id);
    }

}
