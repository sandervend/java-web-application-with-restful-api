package com.feel.backend.api;

import com.feel.backend.controller.EmotionController;
import com.feel.backend.model.Emotion;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.glassfish.jersey.server.JSONP;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by S on 09.04.2016.
 */

@Path("emotions")
@Produces(MediaType.APPLICATION_JSON)
public class EmotionApi {
    private static final String ITEMS_URL = "/api/emotions";

    @GET
    @JSONP(queryParam = "callback")
    public String getAllEmotions(@QueryParam("callback") String callback) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_DEFAULT);
        List<Emotion> emotions = EmotionController.getInstance().getAllEmotions();
        for (Emotion Emotion : emotions) {
            Emotion.setLink(ITEMS_URL + "/" + Emotion.getId());
        }
        return mapper.writeValueAsString(emotions);
    }

    @DELETE
    @JSONP(queryParam = "callback")
    public void deleteAllEmotions() throws Exception {
        EmotionController.getInstance().deleteAllEmotions();
    }

    @GET
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public String getEmotion(@PathParam("id") int id) throws Exception {
        Emotion Emotion = EmotionController.getInstance().getEmotion(id);
        return new ObjectMapper().writeValueAsString(Emotion);
    }

    @PUT
    @JSONP(queryParam = "callback")
    public void putEmotion(String EmotionJson) throws Exception {
        Emotion emotion = new ObjectMapper().readValue(EmotionJson, Emotion.class);
        EmotionController.getInstance().saveOrUpdate(emotion);
    }

    @DELETE
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public void deleteEmotion(@PathParam("id") int id) throws Exception {
        EmotionController.getInstance().deleteEmotion(id);
    }

}
