package com.feel.backend.api;

import com.feel.backend.controller.UserContentItemController;
import com.feel.backend.model.UserContentItem;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.glassfish.jersey.server.JSONP;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by S on 09.04.2016.
 */

@Path("uci")
@Produces(MediaType.APPLICATION_JSON)
public class UserContentItemApi {
    private static final String ITEMS_URL = "/api/uci";

    @GET
    @JSONP(queryParam = "callback")
    public String getAllUserContentItems(@QueryParam("callback") String callback) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_DEFAULT);
        List<UserContentItem> userContentItems = UserContentItemController.getInstance().getAllContentItems();
        for (UserContentItem UserContentItem : userContentItems) {
            UserContentItem.setLink(ITEMS_URL + "/" + UserContentItem.getId());
        }
        return mapper.writeValueAsString(userContentItems);
    }

    @DELETE
    @JSONP(queryParam = "callback")
    public void deleteAllUserContentItems() throws Exception {
        UserContentItemController.getInstance().deleteAllUserContentItem();
    }

    @GET
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public String getUserContentItem(@PathParam("id") int id) throws Exception {
        UserContentItem UserContentItem = UserContentItemController.getInstance().getContentItem(id);
        return new ObjectMapper().writeValueAsString(UserContentItem);
    }

    @PUT
    @JSONP(queryParam = "callback")
    public void putUserContentItem(String UserContentItemJson) throws Exception {
        UserContentItem userContentItem = new ObjectMapper().readValue(UserContentItemJson, UserContentItem.class);
        UserContentItemController.getInstance().saveOrUpdate(userContentItem);
    }

    @DELETE
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public void deleteUserContentItem(@PathParam("id") int id) throws Exception {
        UserContentItemController.getInstance().deleteContentItem(id);
    }

}
