package com.feel.backend.controller;

import com.feel.backend.db.HibernateUtil;
import com.feel.backend.model.Content;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Sander Rõuk on 12.05.2016.
 */
public class ContentController {

    private static volatile ContentController instance = null;

    public static ContentController getInstance() {
        if (instance == null) {
            instance = new ContentController();
        }
        return instance;
    }

    public void saveOrUpdate(Content content) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.saveOrUpdate(content);
        session.flush();
        session.close();

    }

    public void deleteAllContent() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.createQuery("DELETE FROM Content").executeUpdate();
        session.close();
    }

    public List<Content> getAllContent(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Content> contents = (List<Content>) session.createQuery("from Content").list();
        session.close();
        return contents;
    }


    public Content getContent(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Content content = (Content) session.get(Content.class, id);
        session.close();
        return content;
    }


    public boolean deleteContent(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Content content = getContent(id);
        if(!content.equals(null)) {
            session.delete(content);
            session.flush();
            session.close();
            return true;
        }
        session.close();
        return false;
    }
}
