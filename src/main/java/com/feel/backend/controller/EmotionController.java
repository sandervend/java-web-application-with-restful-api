package com.feel.backend.controller;

import com.feel.backend.db.HibernateUtil;
import com.feel.backend.model.Emotion;
import org.hibernate.Hibernate;
import org.hibernate.Session;

import java.util.Enumeration;
import java.util.List;

/**
 * Created by Sander Rõuk on 12.05.2016.
 */
public class EmotionController {
    private static volatile EmotionController instance = null;

    public static EmotionController getInstance() {
        if (instance == null) {
            instance = new EmotionController();
        }
        return instance;
    }

    public void saveOrUpdate(Emotion emotion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.saveOrUpdate(emotion);
        session.flush();
        session.close();
    }

    public void deleteAllEmotions() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.createQuery("Delete from Emotion ").executeUpdate();
        session.close();
    }


    public List<Emotion> getAllEmotions() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Emotion> emotions = (List<Emotion>) session.createQuery("from Emotion").list();
        session.close();
        return emotions;
    }

    public Emotion getEmotion(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Emotion emotion = (Emotion) session.get(Emotion.class, id);
        session.close();
        return emotion;
    }

    public boolean deleteEmotion(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Emotion emotion = getEmotion(id);
        if(!emotion.equals(null)){
            session.delete(emotion);
            session.flush();
            session.close();
            return true;
        } return false;
    }
}
