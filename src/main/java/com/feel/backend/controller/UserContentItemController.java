package com.feel.backend.controller;

import com.feel.backend.db.HibernateUtil;
import com.feel.backend.model.UserContentItem;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Sander Rõuk on 12.05.2016.
 */
public class UserContentItemController {

    private static volatile UserContentItemController instance = null;

    public static UserContentItemController getInstance() {
        if (instance == null) {
            instance = new UserContentItemController();
        }
        return instance;
    }

    public void saveOrUpdate(UserContentItem userContentItem) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.saveOrUpdate(userContentItem);
        session.flush();
        session.close();
    }

    public void deleteAllUserContentItem() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.createQuery("Delete from UserContentItem").executeUpdate();
        session.close();
    }

    public List<UserContentItem> getAllContentItems() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<UserContentItem> ucis = (List<UserContentItem>) session.createQuery("from UserContentItem").list();
        session.close();
        return ucis;
    }

    public UserContentItem getContentItem(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserContentItem uci = (UserContentItem) session.get(UserContentItem.class, id);
        session.close();
        return uci;
    }

    public boolean deleteContentItem(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserContentItem uci = getContentItem(id);
        if(!uci.equals(null)) {
            session.delete(uci);
            session.flush();
            session.close();
            return true;
        }
        return false;
    }
}
