package com.feel.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */


@Entity
@Table(name = "Content")
@XmlRootElement
public class Content {
    @Id
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="content")
    private String content;

    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Content() {}




    public Content(int id, String contentTitle, String content) {
        this.id = id;
        this.title = contentTitle;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(o == null || this.getClass() != o.getClass()) return false;
        if(this.id != ((Content) o).getId()) return false;
        if(!this.title.equals(((Content) o).getTitle())) return false;
        if(!this.content.equals(((Content) o).getContent())) return false;
        return true;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Content{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
