package com.feel.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */

@Entity
@Table(name = "Emotion")
@XmlRootElement
public class Emotion {

    @Id
    @Column(name="id")
    private int id;
    @Column(name="emotionName")
    private String emotionName;
    @Column(name="description")
    private String description;

    private String link;

    public Emotion() {
    }

    public Emotion(int id, String emotionName, String description) {
        this.id = id;
        this.emotionName = emotionName;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmotionName() {
        return emotionName;
    }

    public void setEmotionName(String emotionName) {
        this.emotionName = emotionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Emotion emotion = (Emotion) o;

        if (id != emotion.id) return false;
        if (!emotionName.equals(emotion.emotionName)) return false;
        return description.equals(emotion.description);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + emotionName.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }
}
