package com.feel.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */

@Entity
@Table(name="user_content_item")
@XmlRootElement
public class UserContentItem {
    @Id
    @Column(name="id")
    private int id;
    @Column(name="id_user")
    private int user;
    @Column(name="id_content")
    private int content;
    @Column(name="id_emotion")
    private int emotion;
    @Column(name="tstamp")
    private String timestamp;
    @Column(name = "public")
    private boolean publicity = true;

    private String link;

    public UserContentItem() {
    }

    public UserContentItem(int id, int user, int content, int emotion, String timestamp) {
        this.id = id;
        this.user = user;
        this.content = content;
        this.emotion = emotion;
        this.timestamp = timestamp;
    }

    public UserContentItem(int id, int user, int content, int emotion, boolean publicity, String timestamp) {
        this.id = id;
        this.user = user;
        this.content = content;
        this.emotion = emotion;
        this.publicity = publicity;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

    public int getEmotion() {
        return emotion;
    }

    public void setEmotion(int emotion) {
        this.emotion = emotion;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserContentItem that = (UserContentItem) o;

        if (id != that.id) return false;
        if (user != that.user) return false;
        if (content != that.content) return false;
        if (emotion != that.emotion) return false;
        if (publicity != that.publicity) return false;
        return timestamp.equals(that.timestamp);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + user;
        result = 31 * result + content;
        result = 31 * result + emotion;
        result = 31 * result + timestamp.hashCode();
        result = 31 * result + (publicity ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserContentItem{" +
                "id=" + id +
                ", user=" + user +
                ", content=" + content +
                ", emotion=" + emotion +
                ", timestamp='" + timestamp + '\'' +
                ", publicity=" + publicity +
                '}';
    }
}
