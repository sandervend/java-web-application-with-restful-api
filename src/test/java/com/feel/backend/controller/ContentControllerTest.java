package com.feel.backend.controller;

import com.feel.backend.model.Content;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */
public class ContentControllerTest {
    ContentController contentController = new ContentController();
    Content cTestContent = new Content(1, "Title 1", "Content 1");
    Content cTestContent2 = new Content(2, "Title 2", "Content 2");
    Content cTestContent3 = new Content(33, "Title 3", "Content 3");


    @Before
    public void beforeContentControllerTest(){
        contentController.saveOrUpdate(cTestContent);
        contentController.saveOrUpdate(cTestContent2);
        contentController.saveOrUpdate(cTestContent3);
    }

    @After
    public void afterContentControllerTest(){
        contentController.deleteAllContent();
    }

    @Test
    public void contentControllerGetContentShouldGetAllContent(){
        assertThat(contentController.getAllContent().size(), is(equalTo(3)));
        contentController.deleteAllContent();
        contentController.saveOrUpdate(cTestContent);
        assertThat(contentController.getAllContent().size(), is(equalTo(1)));
    }

    @Test
    public void contentControllerGetcontentByIdShouldSucceed(){
        assertThat(contentController.getContent(1).equals(cTestContent), is(equalTo(true)));
        assertThat(contentController.getContent(33).equals(cTestContent3), is(equalTo(true)));
        assertThat(contentController.getContent(2).equals(cTestContent), is(equalTo(false)));
    }

    @Test
    public void contentControllerUpdateFunctionShouldSucceed(){
        assertThat(contentController.getContent(1).equals(cTestContent), is(equalTo(true)));
        Content temp = contentController.getContent(1);
        cTestContent.setTitle("this title is changed");
        cTestContent.setContent("this content is changed");
        contentController.saveOrUpdate(cTestContent);
        assertThat(contentController.getContent(1).equals(temp), is(equalTo(false)));
        Content temp2 = new Content(1, "this title is changed", "this content is changed");
        assertThat(contentController.getContent(1).equals(temp2), is(equalTo(true)));
    }

    @Test
    public void contentDeleteByIdShouldSucceed(){
        contentController.deleteContent(1);
        assertThat(contentController.getAllContent().size(), is(equalTo(2)));
        contentController.deleteContent(2);
        assertThat(contentController.getAllContent().size(), is(equalTo(1)));
    }

}
