package com.feel.backend.controller;

import com.feel.backend.model.Emotion;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */
public class EmotionControllerTest {
    EmotionController emotionController = new EmotionController();
    Emotion cTestEmotion = new Emotion(1, "Title 1", "Emotion 1");
    Emotion cTestEmotion2 = new Emotion(2, "Title 2", "Emotion 2");
    Emotion cTestEmotion3 = new Emotion(33, "Title 3", "Emotion 3");


    @Before
    public void beforeEmotionControllerTest(){
        emotionController.saveOrUpdate(cTestEmotion);
        emotionController.saveOrUpdate(cTestEmotion2);
        emotionController.saveOrUpdate(cTestEmotion3);
    }

    @After
    public void afterEmotionControllerTest(){
        emotionController.deleteAllEmotions();
    }

    @Test
    public void emotionControllerGetEmotionShouldGetAllEmotion(){
        assertThat(emotionController.getAllEmotions().size(), is(equalTo(3)));
        emotionController.deleteAllEmotions();
        emotionController.saveOrUpdate(cTestEmotion);
        assertThat(emotionController.getAllEmotions().size(), is(equalTo(1)));
    }

    @Test
    public void emotionControllerGetemotionByIdShouldSucceed(){
        assertThat(emotionController.getEmotion(1).equals(cTestEmotion), is(equalTo(true)));
        assertThat(emotionController.getEmotion(33).equals(cTestEmotion3), is(equalTo(true)));
        assertThat(emotionController.getEmotion(2).equals(cTestEmotion), is(equalTo(false)));
    }

    @Test
    public void emotionControllerUpdateFunctionShouldSucceed(){
        assertThat(emotionController.getEmotion(1).equals(cTestEmotion), is(equalTo(true)));
        Emotion temp = emotionController.getEmotion(1);
        cTestEmotion.setEmotionName("this title is changed");
        cTestEmotion.setDescription("this emotion is changed");
        emotionController.saveOrUpdate(cTestEmotion);
        assertThat(emotionController.getEmotion(1).equals(temp), is(equalTo(false)));
        Emotion temp2 = new Emotion(1, "this title is changed", "this emotion is changed");
        assertThat(emotionController.getEmotion(1).equals(temp2), is(equalTo(true)));
    }

    @Test
    public void emotionDeleteByIdShouldSucceed(){
        emotionController.deleteEmotion(1);
        assertThat(emotionController.getAllEmotions().size(), is(equalTo(2)));
        emotionController.deleteEmotion(2);
        assertThat(emotionController.getAllEmotions().size(), is(equalTo(1)));
    }

}
