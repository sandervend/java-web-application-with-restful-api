package com.feel.backend.controller;

import com.feel.backend.model.User;
import com.feel.backend.model.UserContentItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sander Rõuk on 12.05.2016.
 */
public class UserContentItemControllerTest {
    UserContentItemController userContentItemController = new UserContentItemController();
    UserContentItem cTestUserContentItem = new UserContentItem(1,1,1,1,"2008-01-01 00:00:01");
    UserContentItem cTestUserContentItem2 = new UserContentItem(2,2,2,2,"2008-01-01 00:00:02");
    UserContentItem cTestUserContentItem3 = new UserContentItem(50,3,3,3, "2008-01-01 00:00:03");


    @Before
    public void beforeUserContentItemControllerTest(){
        userContentItemController.saveOrUpdate(cTestUserContentItem);
        userContentItemController.saveOrUpdate(cTestUserContentItem2);
        userContentItemController.saveOrUpdate(cTestUserContentItem3);
    }

    @After
    public void afterUserContentItemControllerTest(){
        userContentItemController.deleteAllUserContentItem();
    }

    @Test
    public void getAllContentItemsShouldSucceed(){
        assertThat(userContentItemController.getAllContentItems().size(), is(equalTo(3)));
    }

    @Test
    public void getContentItemById(){
        UserContentItem uci = userContentItemController.getContentItem(1);
        assertThat(uci.equals(cTestUserContentItem), is(equalTo(true)));
        uci = userContentItemController.getContentItem(2);
        assertThat(uci.equals(cTestUserContentItem2), is(equalTo(true)));
        uci = userContentItemController.getContentItem(50);
        assertThat(uci.equals(cTestUserContentItem3), is(equalTo(true)));
    }

    @Test
    public void addUsingUpdate(){
        assertThat(userContentItemController.getAllContentItems().size(), is(equalTo(3)));
        UserContentItem uci = new UserContentItem(4,4,4,4, "2008-01-01 00:00:04");
        userContentItemController.saveOrUpdate(uci);
        assertThat(userContentItemController.getAllContentItems().size(), is(equalTo(4)));
    }

    @Test
    public void deleteById(){
        assertThat(userContentItemController.getAllContentItems().size(), is(equalTo(3)));
        userContentItemController.deleteContentItem(50);
        assertThat(userContentItemController.getAllContentItems().size(), is(equalTo(2)));
        userContentItemController.deleteContentItem(2);
        assertThat(userContentItemController.getAllContentItems().size(), is(equalTo(1)));
    }

}
