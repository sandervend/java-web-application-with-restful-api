package com.feel.backend.model;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */
public class ContentTest {
    private Content cExpect = new Content(1, "ContentTitle", "Content stuff");

    @Test
    public void equalsTestShouldFailIfAllDifferent(){
        Content testContent = new Content(2, "blah", "different blah");
        assertThat(testContent, is(not(equalTo(cExpect))));
    }

    @Test
    public void equalsTestShouldFailIfIdDifferent(){
        Content testContent = new Content(2, "ContentTitle", "Content stuff");
        assertThat(testContent, is(not(equalTo(cExpect))));
    }

    @Test
    public void equalsTestShouldFailIfTitleDifferent(){
        Content testContent = new Content(1, "Diff", "Content stuff");
        assertThat(testContent, is(not(equalTo(cExpect))));
    }

    @Test
    public void equalsTestShouldFailIfContentDifferent(){
        Content testContent = new Content(1, "ContentTitle", "Diff");
        assertThat(testContent, is(not(equalTo(cExpect))));
    }

    @Test
    public void equalsTestShouldSucceed(){
        Content testContent = new Content(1, "ContentTitle", "Content stuff");
        assertThat(testContent, equalTo(cExpect));

    }




}
