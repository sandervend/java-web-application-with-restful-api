package com.feel.backend.model;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */
public class EmotionTest {
    private Emotion eExpect = new Emotion(1, "Happy", "This emotion contains happy thoughts");

    @Test
    public void equalsShouldFailIfAllDifferent(){
        Emotion testEmotion = new Emotion(2, "Sadness", "The emotion of the sad");
        assertThat(testEmotion, is(not(equalTo(eExpect))));
    }

    @Test
    public void equalsShouldFailIfIdDifferent(){
        Emotion testEmotion = new Emotion(2, "Happy", "This emotion contains happy thoughts");
        assertThat(testEmotion, is(not(equalTo(eExpect))));
    }

    @Test
    public void equalsShouldFailIfEmotionNameDifferent(){
        Emotion testEmotion = new Emotion(1, "Diff", "This emotion contains happy thoughts");
        assertThat(testEmotion, is(not(equalTo(eExpect))));
    }

    @Test
    public void equalsShouldFailIfDescriptionDifferent(){
        Emotion testEmotion = new Emotion(1, "Happy", "Diff");
        assertThat(testEmotion, is(not(equalTo(eExpect))));
    }

    @Test
    public void equalsShouldSucceed(){
        Emotion testEmotion = new Emotion(1, "Happy", "This emotion contains happy thoughts");
        assertThat(testEmotion, is(equalTo(eExpect)));
    }

}
