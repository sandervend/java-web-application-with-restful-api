package com.feel.backend.model;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Sander Rõuk on 11.05.2016.
 */
public class UserContentItemTest {
    private UserContentItem uciExpect = new UserContentItem(1, 1, 1, 1, true, "timeStampString");

    @Test
    public void equalsShouldFailIfAllDifferent(){
        UserContentItem testUci = new UserContentItem(2, 2, 2, 2, false, "DIFFERENT");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldFailIfIdDifferent(){
        UserContentItem testUci = new UserContentItem(2, 1, 1, 1, true, "timeStampString");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldFailIfUserDifferent(){
        UserContentItem testUci = new UserContentItem(1, 2, 1, 1, true, "timeStampString");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldFailIfContentDifferent(){
        UserContentItem testUci = new UserContentItem(1, 1, 2, 1, true, "timeStampString");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldFailIfEmotionDifferent(){
        UserContentItem testUci = new UserContentItem(1, 1, 1, 2, true, "timeStampString");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldFailIfTstampDifferent(){
        UserContentItem testUci = new UserContentItem(1, 1, 1, 1, true, "DIFFERENT");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldFailIfPublicityDifferent() {
        UserContentItem testUci = new UserContentItem(1, 1, 1, 1, false, "timeStampString");
        assertThat(testUci, is(not(equalTo(uciExpect))));
    }

    @Test
    public void equalsShouldSucceed(){
        UserContentItem testUci = new UserContentItem(1, 1, 1, 1, true, "timeStampString");
        assertThat(testUci, is(equalTo(uciExpect)));
    }
}
